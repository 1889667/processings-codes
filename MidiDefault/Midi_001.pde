import themidibus.*;

MidiBus myBus;

void setup() {
  size(400, 400); 
  background(255);
  MidiBus.list();
  myBus = new MidiBus(this, 3, 1);
}

void draw() {

  int channel = 0;
  int number = 0;
  int value = 0;
  myBus.sendControllerChange(channel, number, value); 
  delay(15);
}

void controllerChange (int channel, int number, int value) {
  //println(channel);
  //println(number);
  println(value);
  rect(0, value, width, value);
  ellipse(width/2, height/2, value*3, value*3);
}

void delay(int time) {
  int current = millis();
  while (millis () < current+time) Thread.yield();
}
