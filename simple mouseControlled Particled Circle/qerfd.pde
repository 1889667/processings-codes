// SINE COSINE 02
// USING A SINE / COSINE FUNCTION TO ADRESS POINTS IN A CIRCULAR COORDINATE SYSTEM

// instead of the x,y coordinate system, we can now use a circular
// coordinate system in which points are adressed with an angle and a radius

float step = 1;
// int key = 5;

void setup() {
  //size(1200, 800);
  fullScreen();
  background(0);
  noStroke();
  frameRate(25);
}

void draw() {
   fill(0, 20);
  rect(0, 0, width, height);
  //background(255);
  //background(0);
  //fadeout
  translate(width/2,height/2);
  scale(0.4);
 

  fill(0, random(20));

  // a for loop in which 1 ellipse is drawn for each degree step of a circle
  for (int i = 0; i<360; i+=step) {

    float angle = radians(i);
    //float angle = radians(i+frameCount);

    // different ways of using the radius
    //float radius = 150;
    //float radius = i/2.0;
    //float radius = 150 + random(-mouseX, mouseX);
    //float radius = 150 + sin(angle*2+radians(frameCount)) * 10;
    //float radius = 150 + noise(i/50.0 + frameCount/100.0)*mouseX - mouseX/2;

    // seamless noise values at the 360° / 0° position of the circle
    // explanatory gif by Golan Levin: http://gph.is/2Ah5kqG (circle through 2D noise for a perfect loop) 
    float circleX = cos(angle);
    float circleY = sin(angle);
    float a = frameCount/100.0;
    float n = noise(circleX + a, circleY + a)-0.5;

    float radius = 150 + n * random(50, 500) * random(mouseX) ;

    float x = width/2  + cos(angle) * radius;
    float y = height/2 + sin(angle) * radius;

    float o = width/2 + randomGaussian() * 50;
    float p = height/2 + randomGaussian() * 50;
    //ellipse(o,p,2,2); 
    fill(255);
    // try this one together with the fixed radius of 150

    //float y = height/2 + sin(angle + frameCount/50.0) * radius;
    fill(0, random(204), 40);

    int v2 = 15;
    if (keyPressed) {
      if (key=='n') {
        v2 = 50;
      }
    }

    if (keyPressed) {
      if (key=='m') {
        v2 = 150;
      }
    }
    for ( int v = 0; v<v2; v++) {
      if (frameCount>2000 & mouseX==0) rect(x, y, v, step);
      ellipse(x, y, step+random(50), step/5);

      if (keyPressed) {
        if (key=='x') {
          rect(x+v, y+v, random(step*2), random(step*2));
        }
        //if(frameCount>500) rect(x+v+5,y+v+5,step*5/10*v,step*5/10*v);
      }
      rotate(randomGaussian()*2.44/mouseY);

      if (frameCount>150 & frameCount<225) rotate(randomGaussian()*2.44/100);


      //println(key);
      if (keyPressed) {
        if (key=='c') {
          translate(random(10), random(5));
        }
      }
      fill(mouseX, frameCount, mouseY);
      if (mousePressed==true)    fill(mouseX, 0, mouseY);

      if (keyPressed) {
        if (key=='v') {
          translate(random(50), random(50));
          fill(185, 0, 0);
        }
      }
      //ellipse(sin(radians(random(x))),cos(radians(random(y))),step,step);
      //if(keyPressed==true) fill(random(120,240),0,random(0,120));
      //fill(random(138),0,randomGaussian());
    }
  }

  if (keyPressed) {
    if (key=='ö') {
      saveFrame("saves/###.png");
    }
  }
}
