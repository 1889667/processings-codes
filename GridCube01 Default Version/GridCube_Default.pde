import peasy.PeasyCam; 
PeasyCam cam;

void setup () {
  size(1400, 800, P3D);
  background(255);
  //camera();
  cam = new PeasyCam(this, 1000);
}

void draw() {
  background(255);
grids(5);

}

void grids(int mult2) {
  
    pushMatrix();
  grid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(90));
  translate(0, 0, 0);
  grid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(90));
  translate(0, 0, mult2*100);
  grid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(0));
  translate(0, 0, -mult2*100);
  grid(mult2);
  popMatrix();

  pushMatrix();
  rotateX(radians(-90));
  translate(0, 0, 0);
  grid(mult2);
  popMatrix();

  pushMatrix();
  rotateX(radians(-90));
  translate(0, 0, mult2*100);
  grid(mult2);
  popMatrix();
}
