void vgrids(int mult2) {
  pushMatrix();
  vgrid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(90));
  translate(0, 0, 0);
  vgrid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(90));
  translate(0, 0, mult2*100+-20);
  vgrid(mult2);
  popMatrix();

  pushMatrix();
  rotateY(radians(0));
  translate(0, 0, -mult2*100+20);
  vgrid(mult2);
  popMatrix();

  pushMatrix();
  rotateX(radians(-90));
  translate(0, 0, 0);
  vgrid(mult2);
  popMatrix();

  pushMatrix();
  rotateX(radians(-90));
  translate(0, 0, mult2*100+-20);
  vgrid(mult2);
  popMatrix();
}
