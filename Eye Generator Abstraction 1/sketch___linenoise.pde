int stepLength = 30;
int stepNum = 15;

int[] x = {0};
int[] y = {0};
int counter = 0; 
int num = 150;
int b[] = new int[num];
int n[] = new int[num];
int d[] = new int[num];

void setup() {
  size(1200, 800);
  //fullScreen();
  frameRate(180);
  background(0);


  for (int i = 0; i< x.length; i++) {
    b[i]=(int)random(width);
    n[i]=(int)random(height);
    d[i]=(int)random(30);
  }

  x[0] = width/2;
  y[0] = height/2;
}



void draw() {
  translate(width/2, height/2);   // setzt den Kreis in die Mitte des Bildes
  if(frameCount<1800) scale(0.90*frameCount/14000+0.1) ;
  if (frameCount>=1800) scale(0.90*1800/14000+0.1);
  

  println(1*frameCount*0.00005);
  surface.setTitle("frameRate: " + frameRate + "frameCount: " + frameCount ); //" " makes Words a string


  //if(counter%2 == 0) background(0);

  int lastX = x[x.length-1];
  int lastY = y[y.length-1];

  int newX = lastX + (int)random(-stepLength, stepLength)+(int)random(-4, 4);
  int newY = lastY + (int)random(-stepLength, stepLength)+(int)random(-4, 4);

  if (newX<0) newX = 0;
  else if (newX>width) newX = width;
  if (newY<0) newY = 0;
  else if (newY>height) newY = height;

  x = append(x, newX);
  y = append(y, newY);

  if (x.length > stepNum) {
    x = subset(x, 1);
    y = subset(y, 1);
  }
  for (int u = 0; u< x.length; u++) {
    float X = map(sin(frameCount/50.0), -1, 1, b[u], width/2);  
    float Y = map(sin(frameCount/50.0), -1, 1, n[u], height/2); 
    for (int i=0; i<x.length-1; i++) { 
      //line((x[i]), (y[i]), (x[i+1]), (y[i+1]));
      //line(random(random(x[i]),50), random(random((y[i])),50), random(random((x[i+1]))), random(random((y[i+1]))));
      //stroke(0);
      //if (frameCount>=300) fill(random(0, 255));
      strokeWeight(random(1, 15));

      //line(x[i], y[i], x[i+1], y[i+1]);

      //stroke(random(0, 2), random(5, 100));

      
      //if(frameCount>2500) stroke(random(175),0,0);

      if(frameCount>=3000) {
        line(x[i]+2, y[i]+2, x[i+1]+random(50, 100), y[i+1]+random(50, 100)) ;
        strokeWeight(30*noise(random(0, 500)));
        stroke(15, 0, random(150, 200));
        if (frameCount>=2500) stroke(0, 0, frameCount/3000);
        //if (frameCount>=6000) stroke(255-frameCount/100);
      }
      //if (frameCount%2==0) {
      ellipse(X+300+frameCount/20, Y+300+frameCount/20, random(0, 10), random(0, 7));
      //fill(random(15, 150), 0, 0);
      //if (frameCount>300) fill(0, 0, frameCount/39);
if(frameCount>3) {
  if(frameCount>3000) stroke(noise(5,50,500),0,0);
          if(frameCount>4) {      stroke(frameCount/30*noise(random(1000)),0,0);

          strokeWeight(random(0.01,5));
                  line(random(width),random(height),random(5)*noise(X,Y),random(5)*noise(Y,X));
          }

        line(random(width),random(height),noise(X,Y),noise(Y,X));
}

      fill(random(220,255));
      if (frameCount%2==0) ellipse(frameCount/20+X+400, frameCount/20+Y+400, random(50), random(50));
      quad(75+(x[i])/10+frameCount/10, 75+x[i+1]/8+frameCount/10, 75+(x[i])/8+frameCount/10, 75+x[i]/8+frameCount/10, 75+(y[i])/8+frameCount/10, 75+ y[i+1]/8+frameCount/10, 75+y[i]/8+frameCount/10, 75+ y[i]/8+frameCount/10);
      if(frameCount>3) {
        line(random(width),random(height),noise(X,Y),noise(Y,X));
        //stroke(random(255),random(255));
      }
      //stroke(random(0, 15));
      //stroke(0, 0, 156);
      rotate(radians(random(frameCount/2)));
      //translate(random(20), random(20));
    }
  }



  //arc(x[x.length-1]+20, y[y.length-1]+20, 10, 10, 0, HALF_PI);
  //arc(x[x.length-1]+20, y[y.length-1]+20, 20, 20, HALF_PI, PI);
  //arc(x[x.length-1]+20, y[y.length-1]+20, 30, 30, PI, PI+QUARTER_PI);
  //arc(x[x.length-1]+20, y[y.length-1]+20, 25, 25, PI+QUARTER_PI, TWO_PI);


  counter = counter + 1;
  //ellipse(x[x.length-1], y[y.length-1], 10,10);
  //saveFrame("save/####.png");
}
