int stepLength = 30;
int stepNum = 10;

int[] x = {0};
int[] y = {0};
int counter = 0; 

void setup() {
  size(800, 800);
  frameRate(30);
  background(0);

  x[0] = width/2;
  y[0] = height/2;
}



void draw() {
  //if(counter%2 == 0) background(0);

  int lastX = x[x.length-1];
  int lastY = y[y.length-1];

  int newX = lastX + (int)random(-stepLength, stepLength);
  int newY = lastY + (int)random(-stepLength, stepLength);

  if (newX<0) newX = 0;
  else if (newX>width) newX = width;
  if (newY<0) newY = 0;
  else if (newY>height) newY = height;

  x = append(x, newX);
  y = append(y, newY);

  if (x.length > stepNum) {
    x = subset(x, 1);
    y = subset(y, 1);
  }

  for (int i=0; i<x.length-1; i++) { 
    line(x[i], y[i], x[i+1], y[i+1]);
    stroke(0);
    if (frameCount>=60) fill(random(0, 255));
    strokeWeight(random(1, 15));
    line(x[i], y[i], x[i+1], y[i+1]);
    ellipse(x[i], y[i], 5, 5);
    stroke(random(255), 0, 0);
   
    
    //stroke(255, 0, frameCount/4);
    if(frameCount%2==0)line(x[i]+2, y[i]+2, x[i+1]+10, y[i+1]+10) ;
  }
stroke(200);
strokeWeight(random(0.05,2));
noFill();
  arc(x[x.length-1]+random(20,40), y[y.length-1]+20, 10, 10, 0, HALF_PI);
  arc(x[x.length-1]+20, y[y.length-1]+20, 20, 20, HALF_PI, PI);
 


  counter = counter + 1;
  //ellipse(x[x.length-1], y[y.length-1], 10,10);
 //saveFrame("save/####.png");
}
